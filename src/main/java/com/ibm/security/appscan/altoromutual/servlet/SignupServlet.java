package com.ibm.security.appscan.altoromutual.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ibm.security.appscan.Log4AltoroJ;
import com.ibm.security.appscan.altoromutual.util.DBUtil;
import com.ibm.security.appscan.altoromutual.util.ServletUtil;

import static java.lang.System.out;

@WebServlet("/SignupServlet")
public class SignupServlet extends HttpServlet{
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignupServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //log out
        try {
            HttpSession session = request.getSession(false);
            session.removeAttribute(ServletUtil.SESSION_ATTR_USER);
        } catch (Exception e){
            // do nothing
        } finally {
            response.sendRedirect("index.jsp");
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Sign up
        // Create session if there isn't one:
        HttpSession session = request.getSession(true);

        String username = null;

        try {
            username = request.getParameter("userID");
            if (username != null)
                username = username.trim().toLowerCase();

            String password = request.getParameter("passw");
            //password = password.trim().toLowerCase(); //in real life the password usually is case sensitive and this cast would not be done

            String refill = request.getParameter("refill");
            //refill = refill.trim().toLowerCase()
            String firstname = request.getParameter("firstname");
            String lastname = request.getParameter("lastname");
            String rolestring = request.getParameter("rolestring");


            if (DBUtil.isAlreadyUser(username)) {
                Log4AltoroJ.getInstance().SignupError("Sign up failed >>> User: " + username + " >>> Password: " + password);
                throw new Exception("Sign up Failed: We're sorry, but this username was already in our database. Please try again.");
            }

            if (password.compareTo(refill) != 0) {
                Log4AltoroJ.getInstance().SignupError("Sign up failed >>> Password does not match");
                throw new Exception("Sign up Failed: Your confirm password does not match your password.");
            }

            DBUtil.createNewUser(username,password,firstname,lastname,rolestring);



        } catch (Exception ex) {
            request.getSession(true).setAttribute("loginError", ex.getLocalizedMessage());
            response.sendRedirect("login.jsp");
            return;
        }

        response.sendRedirect("login.jsp");
        return;
    }
}
